(defproject learning-webdev "0.1.0-SNAPSHOT"

  :url "https://www.github.com/re-view/re-view"

  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.7.1"

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.238"]
                 [re-view "0.4.15"]]

  :source-paths ["src"])
