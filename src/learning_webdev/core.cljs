(ns learning-webdev.core
  (:require
   [re-view.core :as v :refer [defview]]))

(defview TodoItem
  ;; this is the 'Methods map', remember?
  ;; in this case, it only contains the `:key` keyword
  ;; and it's important because we need a unique identifier
  ;; for each todo item
  ;; See https://re-view.io/docs/re-view/getting-started#__special-keys
  {:key (fn [_ {:keys [id]}]
          id)}
  
  [;; `props` has a single key `:on-click`
   {:keys [on-click]}

   ;; ...and the second argument is the todo item
   {:keys [text done?] :as todo-item}]
  
  [:li (merge {:on-click #(on-click todo-item)}
              
              (when done?
                {:style {:text-decoration "line-through"}}))
   text])


(defn todo-or-not-todo [todo-item]
  (update todo-item :done? not))


(defview TodoList
  {:view/initial-state {}}
  
  [{:keys [:view/state]}] 
  [:div 
   [:ul
    (for [[_ {:keys [id] :as todo-item}] @state]
      (TodoItem {:on-click #(swap! state assoc id (todo-or-not-todo %))} todo-item))]
   
   [:input {:on-key-down (fn [event]
                           (when (= (.-keyCode event) 13)
                             (let [new-id            (random-uuid)
                                   
                                   new-todo-item {:id       new-id
                                                  :text     (.. event -target -value)}]
                               (swap! state assoc new-id new-todo-item))))}]])



(defview Layout [this]
  (TodoList))


(defn ^:export render []
  (v/render-to-dom (Layout) "learning-webdev"))
